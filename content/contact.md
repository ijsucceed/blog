---
title: Contact
date: 2018-09-17 05:14:48 +0000

---

As a freelance web developer, I will love to hear from you.

Need a website? or want to work with me. You can chat me up on <a href="t.me/ijsucceed">Telegram</a>. 

I am also on <a href="https://twitter.com/ijsucceed">twitter</a>.

Feel free to ask me any question on web development. I will love to help.

For all other inquiries, send me a mail.

*Ikwuje24@gmail.com*

